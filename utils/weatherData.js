const request = require('request')

const constants = require('../config')


const weatherData = (address, callback) => {

    const url = constants.openWeatherMap.BASE_URL + encodeURIComponent(address) + '&appid=' + constants.openWeatherMap.SECRET_KEY;
    console.log(url);
    // callback(true);  

    request({ url, json: true }, (error, { body }) => {
        if (error) {
            callback('Can not Able to access', undefined)
        } else {
            callback(undefined, {
                temperature: body.main.temp,
                description: body.weather[0].description,
                cityName: body.name
            })
        }
    })
}

module.exports = weatherData;