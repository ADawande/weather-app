const constants = {
    openWeatherMap: {
        BASE_URL: "https://api.openweathermap.org/data/2.5/weather?q=",
        SECRET_KEY: "a7920881d8b0a6ccedc92367aad5e831"
    }
}

module.exports = constants;