var fetchWeather = ('/weather')

const weatherForm = document.querySelector('form'); //form for submit button
const search = document.querySelector('input'); // form for input button

const weatherIcon = document.querySelector('.weatherIcon i');
const weatherCondition = document.querySelector('.weatherCondition');
const tempElement = document.querySelector('.temperature span');
const locationElement = document.querySelector('.place');

const dateElement = document.querySelector('.date');

const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "Octomber", "November", "December"];

dateElement.textContent = new Date().getDate() + " , " + monthNames[new Date().getMonth()].substring(0, 3);

weatherForm.addEventListener('submit', (event) => {

    event.preventDefault();

    locationElement.textContent = "Loading...."
    tempElement.textContent = ""
    weatherCondition.textContent = ""

    // console.log(search.value);
    // APi Call = /weather?search.value 
    const locationApi = fetchWeather + "?address=" + search.value;

    fetch(locationApi).then(response => {
        response.json().then(data => {
            //   console.log(data);       

            if (data.error) {
                locationElement.textContent = data.error
                tempElement.textContent = ""
                weatherCondition.textContent = ""
            } else {
                locationElement.textContent = data.cityName;
                tempElement.textContent = (data.temperature - 273.5).toFixed(2) + String.fromCharCode(176) + "C";
                weatherCondition.textContent = data.description.toUpperCase();
            }

        })
    })
});