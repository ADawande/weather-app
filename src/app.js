const express = require('express')
const app = express();

const hbs = require('hbs')
const path = require('path')

const weatherData = require('../utils/weatherData')

const port = process.env.PORT || 3000;


// Path for static data folder = Public
const publicStaticDirPath = path.join(__dirname, '../public')
    //Path for partials folder
const partialsPath = path.join(__dirname, '../templates/partials')
    //Path for views folder
const viewsPath = path.join(__dirname, '../templates/views')


// Telling express path of public folder
app.use(express.static(publicStaticDirPath));
//hbs path
app.set('view engine', 'hbs');
//View path
app.set('views', viewsPath);
//use HBS to define patial path
hbs.registerPartials(partialsPath);


app.get('', (req, res) => {
    // res.send('Hi! This is weather app')
    res.render('index', {
        title: ' Weather App'
    })
})

//localhost:3000/weather?address=pue
app.get('/weather', (req, res) => {
    const address = req.query.address

    if (!address) {
        return res.send({
            error: "You must enter address"
        })
    }

    weatherData(address, (error, { temperature, description, cityName }) => {
        if (error) {
            return res.send({
                error
            })
        }
        console.log(temperature, description, cityName);
        res.send({
            temperature,
            description,
            cityName
        })
    })
})

app.get("*", (req, res) => {
    res.send('Page not found')
})


app.listen(port, () => {
    console.log('Server is running and active on port ', port);
})